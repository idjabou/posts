import { Component, OnInit } from '@angular/core';
import {PostService} from '../services/post.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Post} from '../models/Post.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {
  postForm: any;
  constructor(private postService: PostService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(): void {
    this.postForm = this.formBuilder.group(
      {
        title: ['', Validators.required],
        content: '',
        loveIts: 0
      }
    );
  }
  onSavePost(): void {
    const title = this.postForm.get('title').value;
    const content = this.postForm.get('content').value;
    const newPost = new Post(title, content, 0);
    this.postService.addPost(newPost);
    this.router.navigate(['posts']);
  }

}
