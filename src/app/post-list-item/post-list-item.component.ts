import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../models/Post.model';
import {PostService} from '../services/post.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() post: any;
  constructor(private postService: PostService) { }

  ngOnInit(): void { }
  
  onLoveIts(): void {
    if (this.post) {
      this.post.loveIts++;
      this.postService.saveInServer();
    }
  }
  onDontLoveIts(): void {
    if (this.post) {
      this.post.loveIts--;
      this.postService.saveInServer();
    }
  }
  onRemove(): void {
    if (this.post) {
      this.postService.removePost(this.post);
    }
  }
}
