import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Post} from '../models/Post.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  firebaseUrl = 'https://http-client-demo-c261c-default-rtdb.firebaseio.com/posts.json';
  posts: Post[] = [];
  postSubject = new Subject<Post[]>();
  objectErro = {};

  /**
   * Constructor
   * @param httpClient
   */
  constructor(private httpClient: HttpClient, private router: Router) { }
  /**
   * Fait le dispatch sur l'ecoute du subject en cas d'evenement
   * @return void
   */
  emitPosts(): void {
    if (this.posts) {
      this.postSubject.next(this.posts.slice());
    }
  }
  /**
   *  Recupère les posts depuis firebase
   * @return any[]
   */
  getPostFromServer(): any[] {
    this.httpClient.get<any[]>(this.firebaseUrl).subscribe(
      (response) => {
        if (response) {
          this.posts = response;
          this.emitPosts();
        }
      },
      (error) => {
        this.objectErro = {
          code: -1,
          msg: error
        };
      }
    );
    return this.posts;
  }
  /**
   * Ajout d'un nouveau post
   * @param post
   * @return any
   */
  addPost(post: Post): any {
    this.posts.push(post);
    this.saveInServer();
    this.emitPosts();
  }
  /**
   * Supprime tous les posts
   */
  removeAll(): void {
    this.posts = [];
    this.saveInServer();
    this.emitPosts();
  }
  /**
   * Enregistre les modification 
   * 
   */
  saveInServer() {
    this.httpClient.put(this.firebaseUrl, this.posts).subscribe(
      () => {
        console.log('Posts mis à jour à la base !');
        this.objectErro = {
          code: 0,
          msg: 'Enregistrement terminé avec succès !'
        }
        this.emitPosts();
      },
      (error) => {
        this.objectErro = {
          code: -1,
          msg: error
        }
      }
    );
  }
  /**
   * Suppression d'un post
   * @param post 
   * 
   */
  removePost(post: Post): any {
    const postIndex = this.posts.findIndex(
      (postEl) => {
        if (postEl === post) {
          return true;
        }
        return false;
      }
    );
    this.posts.splice(postIndex, 1);
    this.saveInServer();
    this.emitPosts();
    return this.objectErro;
  }
  /**
   * Desinscription au Subject
   * @return void
   */
  unsubscribe(): void {
    this.postSubject.unsubscribe();
  }
}
