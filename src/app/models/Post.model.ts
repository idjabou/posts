export class Post {
  createdAt: any;
  constructor(public title: string, public content: string, public loveIts: number) {
    this.createdAt = new Date();
  }
}
