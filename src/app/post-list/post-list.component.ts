import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {Post} from '../models/Post.model';
import {PostService} from '../services/post.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {
  @Input() posts: Post[] = [];
  postSubscribe: any;

  constructor(private postService: PostService, private router: Router) { }

  ngOnInit(): void {
    this.postSubscribe = this.postService.postSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );

    this.postService.emitPosts();
  }
  onRefreshFromServer(): void {
    this.postService.getPostFromServer();
  }
  onRemoveAllOnServer(): void {
    if (confirm('Etes-vous sûr de vouloir supprimer tous les posts ?')) {
      this.postService.removeAll();
    }
  }
  createPost(): void {
    this.router.navigate(['/posts', 'new']);
  }
  ngOnDestroy(): void {
    this.postSubscribe.unsubscribe();
  }
}
